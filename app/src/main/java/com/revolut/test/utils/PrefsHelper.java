/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.revolut.test.mvp.model.data.ChangeRatesData;

/**
 * Created by trimf on 07/07/07.
 */
public class PrefsHelper {
    private static final String EXTRA_PREFIX = "prefs_";
    private static final String PREF_SETTINGS_FILE_NAME = EXTRA_PREFIX + "prefs.xml";
    private static final String PREFS_CHANGE_RATES = EXTRA_PREFIX + ".change_rates_";

    public synchronized static void saveChangeRates(ChangeRatesData changeRate, String currencyName, Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREF_SETTINGS_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(getPrefsNameForCurrency(currencyName), new Gson().toJson(changeRate));
        editor.commit();
    }

    public static @Nullable
    ChangeRatesData loadChangeRates(String currencyName, Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREF_SETTINGS_FILE_NAME, Context.MODE_PRIVATE);
        if (settings != null) {
            String jsonString = settings.getString(getPrefsNameForCurrency(currencyName), null);
            if(!TextUtils.isEmpty(jsonString)){
                return new Gson().fromJson(jsonString, ChangeRatesData.class);
            }
        }
        return null;
    }

    private static @NonNull String getPrefsNameForCurrency(String currencyName){
        return PREFS_CHANGE_RATES+currencyName;
    }


}

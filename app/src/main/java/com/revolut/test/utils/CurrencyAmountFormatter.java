/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.utils;

import java.text.DecimalFormat;

/**
 * Created by trimf on 07/07/2017.
 */

public class CurrencyAmountFormatter {

    public static String getStringFromFloat(float amount) {
        return new DecimalFormat("#.####").format(amount);
    }

    public static float getFloatFromString(String amount) {
        try{
            return Float.parseFloat(amount);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return 0;
    }
}

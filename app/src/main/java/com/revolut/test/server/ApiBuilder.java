/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.server;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by a.baskakov on 16/08/16.
 */
public class ApiBuilder {

    private static final String FIXER_URL = "http://api.fixer.io/";

    static Api buildRetrofitService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiBuilder.FIXER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(Api.class);
    }
}

/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.server;

import com.revolut.test.mvp.model.data.ChangeRatesData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by a.baskakov on 16/08/16.
 */
public interface Api {

    @GET("latest")
    Call<ChangeRatesData> getChangeRates(@Query("base") String currency);
}

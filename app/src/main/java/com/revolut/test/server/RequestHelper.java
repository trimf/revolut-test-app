/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.server;

import android.support.annotation.NonNull;

import com.revolut.test.mvp.model.ICallback;
import com.revolut.test.mvp.model.IChangeModel;
import com.revolut.test.mvp.model.data.ChangeRatesData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by a.baskakov on 16/08/16.
 */
public class RequestHelper implements IChangeModel {

    private static volatile RequestHelper instance;

    public static RequestHelper getInstance() {
        RequestHelper localInstance = instance;
        if (localInstance == null) {
            synchronized (RequestHelper.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new RequestHelper();
                }
            }
        }
        return localInstance;
    }

    @Override
    public void getChangeRates(final ICallback<ChangeRatesData> callback, String currency) {
        Call<ChangeRatesData> call = ApiBuilder.buildRetrofitService().getChangeRates(currency);

        call.enqueue(new Callback<ChangeRatesData>() {
            @Override
            public void onResponse(@NonNull Call<ChangeRatesData> call, @NonNull Response<ChangeRatesData> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    callback.onFailure(null);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ChangeRatesData> call, @NonNull Throwable t) {
                callback.onFailure(t);
            }
        });
    }
}

/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;

import com.revolut.test.activity.change.view_pager.CurrencyPagerAdapter;

/**
 * Created by trimf on 07/07/2017.
 */

public class CurrencyViewPager extends ViewPagerWithPaginator {
    public CurrencyViewPager(Context context) {
        super(context);
    }

    public CurrencyViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CurrencyViewPager(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void convertTo(@NonNull String currencyName, String amount){
        CurrencyPagerAdapter currencyPagerAdapter = (CurrencyPagerAdapter) mViewPager.getAdapter();
        currencyPagerAdapter.convertTo(currencyName, amount);
    }

    public void convertFrom(@NonNull String currencyName, String amount){
        CurrencyPagerAdapter currencyPagerAdapter = (CurrencyPagerAdapter) mViewPager.getAdapter();
        currencyPagerAdapter.convertFrom(currencyName, amount);
    }
}

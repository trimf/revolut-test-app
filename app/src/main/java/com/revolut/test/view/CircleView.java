/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.revolut.test.R;

/**
 * Created by alexey on 11/02/15.
 */
public class CircleView extends View {

    public static final int STROKE = 0;
    public static final int FILL = 1;
    public static final int FILL_AND_STROKE = 2;

    private Paint mPaint;
    private static final int DEFAULT_WIDTH = 50;
    private static final float DEFAULT_STROKE_WIDTH = 1f;
    private float mWidth = DEFAULT_WIDTH;
    private float mStrokeWidth = DEFAULT_STROKE_WIDTH;
    private int type = STROKE;
    private int color = Color.WHITE;

    public CircleView(Context context) {
        super(context);
        init(context);
    }

    public void setColor(int color) {
        this.color = color;
        invalidate();
        requestLayout();
    }

    public void setType(int type)
    {
        this.type = type;
        mPaint.setStyle(getStyle(type));

        invalidate();
        requestLayout();
    }

    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CircleView,
                0, 0);

        try {
            mStrokeWidth = a.getDimension(R.styleable.CircleView_strokeWidth, DEFAULT_STROKE_WIDTH);
            mWidth = a.getDimension(R.styleable.CircleView_defaultWidth, DEFAULT_WIDTH);
            type = a.getInt(R.styleable.CircleView_type, STROKE);
            color = a.getColor(R.styleable.CircleView_circleViewColor, color);
        } finally {
            a.recycle();
        }
        init(context);
    }

    public CircleView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        mPaint = new Paint();

        mPaint.setStyle(getStyle(type));
        mPaint.setStrokeWidth(mStrokeWidth);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mPaint.setAntiAlias(true);
        mPaint.setColor(color);

        float radius = (mWidth) / 2;
        switch (type){
            case FILL:
                canvas.drawCircle(
                        radius,
                        radius,
                        radius,
                        mPaint
                );
                break;
            case STROKE:
            case FILL_AND_STROKE:
                canvas.drawCircle(
                        radius,
                        radius,
                        radius - mStrokeWidth,
                        mPaint
                );
                break;
        }

    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(
                widthMeasureSpec,
                heightMeasureSpec
        );
        //Get size requested and size mode
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width, height;

        //Determine Width
        switch (widthMode) {
            case MeasureSpec.EXACTLY:
                width = widthSize;
                break;
            case MeasureSpec.AT_MOST:
                width = (int)Math.min(mWidth, widthSize);
                break;
            case MeasureSpec.UNSPECIFIED:
            default:
                width = (int)mWidth;
                break;
        }

        //Determine Height
        switch (heightMode) {
            case MeasureSpec.EXACTLY:
                height = heightSize;
                break;
            case MeasureSpec.AT_MOST:
                height = (int)Math.min(mWidth, heightSize);
                break;
            case MeasureSpec.UNSPECIFIED:
            default:
                height = (int)mWidth;
                break;
        }

        setMeasuredDimension(width, height);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(
                w,
                h,
                oldw,
                oldh
        );
        mWidth = Math.min(w, h);
    }

    private Paint.Style getStyle(int type)
    {
        switch (type) {
            case STROKE:
                return Paint.Style.STROKE;
            case FILL:
                return Paint.Style.FILL;
            case FILL_AND_STROKE:
                return Paint.Style.FILL_AND_STROKE;
            default:
                return Paint.Style.STROKE;
        }
    }
}

/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.revolut.test.R;
import com.revolut.test.activity.change.view_pager.CurrencyPagerAdapter;
import com.revolut.test.activity.change.view_pager.CurrencyPagerItem;
import com.trimf.viewpager.BasePagerAdapter;
import com.trimf.viewpager.FixedSpeedScroller;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by trimf on 06/07/2017.
 */

public class ViewPagerWithPaginator<T extends BasePagerAdapter> extends LinearLayout {
    ViewPager mViewPager;
    Paginator mPaginator;
    CurrencyPagerAdapter mPagerAdapter;

    private Set<ViewPager.OnPageChangeListener> mOnPageChangeListeners;

    public void addOnPageChangeListener(ViewPager.OnPageChangeListener listener){
        if(mOnPageChangeListeners==null){
            mOnPageChangeListeners = new HashSet<>();
        }
        mOnPageChangeListeners.add(listener);
    }

    public void removeOnPageChangeListener(ViewPager.OnPageChangeListener listener){
        if(mOnPageChangeListeners!=null){
            mOnPageChangeListeners.remove(listener);
        }
    }

    public ViewPagerWithPaginator(Context context) {
        super(context);
        init(context);
    }

    public ViewPagerWithPaginator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ViewPagerWithPaginator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context){
        setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.CENTER_VERTICAL);

        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.view_pager_with_paginator, this, true);
        mViewPager = (ViewPager)findViewById(R.id.view_pager);
        mPaginator = (Paginator) findViewById(R.id.paginator);

        //beautiful smooth scrolling
        FixedSpeedScroller.setFixedScroller(mViewPager);
    }

    public CurrencyPagerItem getCurrentPagerItem(){
        return mPagerAdapter.getList().get(mPagerAdapter.getRealPosition(mViewPager.getCurrentItem()));
    }

    public void setPagerAdapter(final CurrencyPagerAdapter pagerAdapter){
        mPagerAdapter = pagerAdapter;
        mViewPager.setAdapter(pagerAdapter);
        mViewPager.setCurrentItem(pagerAdapter.getCount() / 2, false);
        mPaginator.setPageNumber(pagerAdapter.getPageNumber());

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if(mOnPageChangeListeners!=null){
                    for(ViewPager.OnPageChangeListener listener:mOnPageChangeListeners){
                        listener.onPageScrolled(position, positionOffset, positionOffsetPixels);
                    }
                }
            }

            @Override
            public void onPageSelected(int position) {
                mPaginator.setCurPage(pagerAdapter.getCurrentPage(position));
                if(mOnPageChangeListeners!=null){
                    for(ViewPager.OnPageChangeListener listener:mOnPageChangeListeners){
                        listener.onPageSelected(position);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if(mOnPageChangeListeners!=null){
                    for(ViewPager.OnPageChangeListener listener:mOnPageChangeListeners){
                        listener.onPageScrollStateChanged(state);
                    }
                }
            }
        });
    }
}

/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.view;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.revolut.test.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexey on 03/12/15.
 */
public class Paginator extends LinearLayout{
    List<View> pages;


    public Paginator(Context context) {
        super(context);
        init(context);
    }

    public Paginator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public Paginator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context){
        setOrientation(LinearLayout.HORIZONTAL);
        setGravity(Gravity.CENTER);
    }

    public void setPageNumber(int pageNumber) {
        removeAllViews();
        pages = new ArrayList<>();
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        for (int i = 0; i < pageNumber; i++) {
            View page = layoutInflater.inflate(R.layout.paginator_item, null);
            addView(page);
            pages.add(page);
        }
        setCurPage(0);
    }

    public void setCurPage(int cur) {
        for (int i = 0; i < pages.size(); i++) {
            View page = pages.get(i);
            if (i == cur) {
                ViewCompat.setAlpha(page, 1f);
                ViewCompat.setScaleX(page, 1.1f);
                ViewCompat.setScaleY(page, 1.1f);
            } else {
                ViewCompat.setAlpha(page, 0.33f);
                ViewCompat.setScaleX(page, 0.9f);
                ViewCompat.setScaleY(page, 0.9f);
            }
        }
    }
}
/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.activity.change;

/**
 * Created by trimf on 07/07/2017.
 */

public class Currencies {
    public static final String USD = "USD";
    public static final String EUR = "EUR";
    public static final String GBP = "GBP";
}

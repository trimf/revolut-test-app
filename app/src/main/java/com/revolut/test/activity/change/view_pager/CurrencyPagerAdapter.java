/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.activity.change.view_pager;

import android.support.annotation.NonNull;

import com.trimf.viewpager.InfinitePagerAdapter;

import java.util.List;

/**
 * Created by trimf on 06/07/2017.
 */

public class CurrencyPagerAdapter extends InfinitePagerAdapter<CurrencyPagerItem> {
    public CurrencyPagerAdapter(List<CurrencyPagerItem> pagerItems) {
        super(pagerItems);
    }

    public void convertTo(@NonNull String currencyName, String amount){
        for(CurrencyPagerItem currencyPagerItem:mPagerItems){
            currencyPagerItem.convertTo(currencyName, amount);
        }
    }

    public void convertFrom(@NonNull String currencyName, String amount){
        for(CurrencyPagerItem currencyPagerItem:mPagerItems){
            currencyPagerItem.convertFrom(currencyName, amount);
        }
    }
}

/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.activity.change.view_pager;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.revolut.test.R;
import com.revolut.test.mvp.model.data.ChangeRatesData;
import com.revolut.test.utils.CurrencyAmountFormatter;
import com.revolut.test.utils.PrefsHelper;
import com.trimf.viewpager.BasePagerItem;

import java.util.Locale;

import static com.revolut.test.R.id.amount;

/**
 * Created by trimf on 06/07/2017.
 */

public class CurrencyPagerItem extends BasePagerItem implements Cloneable {
    private View mView;
    private TextView mName;
    private EditText mAmount;
    private TextView mChangeRate;
    private String mCurrencyName;

    private boolean mTo = false;
    private String mTargetCurrencyName;
    private String mTargetAmount = "0";

    ICurrencyPagerItemListener mListener;

    public CurrencyPagerItem(Context context, String currencyName, @NonNull ICurrencyPagerItemListener focusListener) {
        super(context);
        mCurrencyName = currencyName;
        mListener = focusListener;
    }

    @Override
    public View getView() {
        if (mView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            mView = inflater.inflate(R.layout.currency_pager_item, null);
            mName = (TextView) mView.findViewById(R.id.name);
            mAmount = (EditText) mView.findViewById(amount);
            mChangeRate = (TextView) mView.findViewById(R.id.change_rate);
            mName.setText(mCurrencyName);

            mAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    mListener.focused(CurrencyPagerItem.this, b);
                }
            });

            mAmount.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (mAmount.isFocused() && !mTo) {
                        mListener.amountChanged(CurrencyPagerItem.this);
                    }
                }
            });
        }
        updateViews();
        return mView;
    }

    public String getAmount() {
        return String.valueOf(mAmount.getText());
    }

    public String getCurrencyName() {
        return mCurrencyName;
    }

    @Override
    public CurrencyPagerItem clone() throws CloneNotSupportedException {
        CurrencyPagerItem currencyPagerItem = (CurrencyPagerItem) super.clone();
        currencyPagerItem.mCurrencyName = mCurrencyName;
        currencyPagerItem.mListener = mListener;
        return currencyPagerItem;
    }

    @Override
    public void destroyView() {
        //we don't destroy views, we have few of them
    }

    public void convertTo(@NonNull String currencyName, String amount) {
        mTo = true;
        mTargetCurrencyName = currencyName;
        mTargetAmount = amount;
        if (mView != null) {
            updateViews();
        }
    }

    public void convertFrom(@NonNull String currencyName, String amount) {
        mTo = false;
        mTargetCurrencyName = currencyName;
        mTargetAmount = amount;
        if (mView != null) {
            updateViews();
        }
    }

    private void updateViews() {
        if (mTo) {
            ChangeRatesData changeRatesData = PrefsHelper.loadChangeRates(mTargetCurrencyName, mContext);
            String rate = null;
            if (mCurrencyName.equals(mTargetCurrencyName)) {
                mChangeRate.setVisibility(View.VISIBLE);
                mChangeRate.setText(String.format(Locale.US,
                        mContext.getString(R.string.change_rate),
                        mTargetCurrencyName,
                        "1",
                        mTargetCurrencyName));
                mAmount.setText(mTargetAmount);
            } else if (changeRatesData != null) {
                rate = changeRatesData.getRateByCurrency(mCurrencyName);
                if (!TextUtils.isEmpty(rate)) {
                    mChangeRate.setVisibility(View.VISIBLE);
                    mChangeRate.setText(String.format(Locale.US,
                            mContext.getString(R.string.change_rate),
                            mTargetCurrencyName,
                            rate,
                            mCurrencyName));
                    float amount = CurrencyAmountFormatter.getFloatFromString(rate) * CurrencyAmountFormatter.getFloatFromString(mTargetAmount);
                    mAmount.setText(CurrencyAmountFormatter.getStringFromFloat(amount));
                } else {
                    mChangeRate.setVisibility(View.INVISIBLE);
                    mAmount.setText("0");
                }
            }else{
                mChangeRate.setVisibility(View.INVISIBLE);
                mAmount.setText("0");
            }
        } else {
            mChangeRate.setVisibility(View.INVISIBLE);
            if (!mCurrencyName.equals(mTargetCurrencyName) && !mAmount.isFocused()) {
                mAmount.setText(mTargetAmount);
            }
        }
    }
}

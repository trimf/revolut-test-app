/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.activity.change;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.revolut.test.R;
import com.revolut.test.activity.change.view_pager.CurrencyPagerAdapter;
import com.revolut.test.activity.change.view_pager.CurrencyPagerItem;
import com.revolut.test.activity.change.view_pager.ICurrencyPagerItemListener;
import com.revolut.test.mvp.presenter.ChangePresenter;
import com.revolut.test.mvp.view.IChangeView;
import com.revolut.test.view.CurrencyViewPager;

import java.util.ArrayList;
import java.util.List;

public class ChangeActivity extends AppCompatActivity implements IChangeView {

    private List<CurrencyViewPager> mCurrencyViewPagers;

    private int mFocusedNumber=-1;

    CurrencyViewPager mTopCurrency;
    CurrencyViewPager mBottomCurrency;

    private ChangePresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter = new ChangePresenter();

        setContentView(R.layout.activity_change);

        mCurrencyViewPagers = new ArrayList<>(2);

        mTopCurrency = (CurrencyViewPager) findViewById(R.id.top_currencty);
        mBottomCurrency = (CurrencyViewPager) findViewById(R.id.bottom_currencty);

        mCurrencyViewPagers.add(mTopCurrency);
        mCurrencyViewPagers.add(mBottomCurrency);

        int size = mCurrencyViewPagers.size();
        //we can have more than 2 view pagers, they will work
        for (int i = 0; i < size; i++) {
            initCurrencyViewPager(mCurrencyViewPagers.get(i), i);
        }
    }

    private void initCurrencyViewPager(CurrencyViewPager currencyViewPager, final int number) {
        ICurrencyPagerItemListener pagerItemListener = new ICurrencyPagerItemListener() {
            @Override
            public void focused(CurrencyPagerItem pagerItem, boolean focused) {
                if (focused && number != mFocusedNumber) {
                    mFocusedNumber = number;
                    mPresenter.pagerFocused(number);
                }
            }

            @Override
            public void amountChanged(CurrencyPagerItem pagerItem) {
                if (number == mFocusedNumber) {
                    mPresenter.amountChanged();
                }
            }
        };
        List<CurrencyPagerItem> currencyList = new ArrayList<>();
        currencyList.add(new CurrencyPagerItem(this, Currencies.USD, pagerItemListener));
        currencyList.add(new CurrencyPagerItem(this, Currencies.EUR, pagerItemListener));
        currencyList.add(new CurrencyPagerItem(this, Currencies.GBP, pagerItemListener));

        currencyViewPager.setPagerAdapter(new CurrencyPagerAdapter(currencyList));
        currencyViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mPresenter.pageSelected(number);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.bindView(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.unbindView();
    }

    @Override
    public void updateChangeRates() {
        if(mFocusedNumber==-1){
            return;
        }
        CurrencyViewPager currencyViewPager = mCurrencyViewPagers.get(mFocusedNumber);
        CurrencyPagerItem currencyPagerItem = currencyViewPager.getCurrentPagerItem();

        if (currencyPagerItem != null) {
            try {
                String amount = currencyPagerItem.getAmount();
                String currencyName = currencyPagerItem.getCurrencyName();
                int size = mCurrencyViewPagers.size();
                for (int i = 0; i < size; i++) {
                    if (i == mFocusedNumber) {
                        mCurrencyViewPagers.get(i).convertFrom(currencyName, amount);
                    } else {
                        mCurrencyViewPagers.get(i).convertTo(currencyName, amount);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}

/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.activity.change.view_pager;

/**
 * Created by trimf on 07/07/2017.
 */

public interface ICurrencyPagerItemListener {
    void focused(CurrencyPagerItem pagerItem, boolean focused);
    void amountChanged(CurrencyPagerItem pagerItem);
}

/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.mvp.presenter;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.revolut.test.App;
import com.revolut.test.activity.change.Currencies;
import com.revolut.test.mvp.model.ICallback;
import com.revolut.test.mvp.model.data.ChangeRatesData;
import com.revolut.test.mvp.view.IChangeView;
import com.revolut.test.server.RequestHelper;
import com.revolut.test.utils.PrefsHelper;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by trimf on 06/07/2017.
 */

public class ChangePresenter extends BasePresenter<IChangeView> implements IChangePresenter {

    private Timer mRequestTimer;
    private static final int TIMER_INTERVAL = 30000;

    Handler mHandler = new Handler();

    @Override
    public void bindView(@NonNull IChangeView view) {
        super.bindView(view);
        startRequestTimer();
    }

    @Override
    public void unbindView() {
        super.unbindView();
        stopRequestTimer();
    }

    private void updateView() {
        if (setupDone()) {
            view().updateChangeRates();
        }
    }

    private void startRequestTimer() {
        stopRequestTimer();
        mRequestTimer = new Timer();
        mRequestTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        //It's a pity that the server does not support loading all data at once
                        //for custom currencies
                        loadChangeRates(Currencies.USD);
                        loadChangeRates(Currencies.EUR);
                        loadChangeRates(Currencies.GBP);

                    }
                });
            }
        }, 0, TIMER_INTERVAL);
    }

    private void loadChangeRates(final String currencyName) {
        RequestHelper.getInstance().getChangeRates(new ICallback<ChangeRatesData>() {
            @Override
            public void onSuccess(@NonNull ChangeRatesData data) {
                changeRatesLoaded(data, currencyName);
            }

            @Override
            public void onFailure(@Nullable Throwable t) {
                //do noting
            }
        }, currencyName);
    }

    private void changeRatesLoaded(ChangeRatesData changeRatesData, String currencyName) {
        ChangeRatesData oldChangeRatesData = PrefsHelper.loadChangeRates(currencyName, App.getContext());
        if (oldChangeRatesData == null || !oldChangeRatesData.equals(changeRatesData)) {
            PrefsHelper.saveChangeRates(changeRatesData, currencyName, App.getContext());
            updateView();
        }

    }

    private void stopRequestTimer() {
        if (mRequestTimer != null) {
            mRequestTimer.cancel();
            mRequestTimer = null;
        }
    }

    @Override
    public void pageSelected(int viewPagerNumber) {
        updateView();
    }

    @Override
    public void pagerFocused(int viewPagerNumber) {
        updateView();
    }

    @Override
    public void amountChanged() {
        updateView();
    }
}

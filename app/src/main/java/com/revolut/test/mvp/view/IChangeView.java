/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.mvp.view;

/**
 * Created by trimf on 06/07/2017.
 */

public interface IChangeView {
    void updateChangeRates();
}

/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.mvp.model.data;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by a.baskakov on 16/08/16.
 */
public class ChangeRatesData implements Serializable {
    public String base;
    public String date;

    HashMap<String, String> rates;
//    public RatesData rates;

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ChangeRatesData) {
            Gson gson = new Gson();
            return gson.toJson(this).equals(gson.toJson(obj));
        }
        return false;
    }

    public String getRateByCurrency(String currencyName){
        if(rates!=null){
            return rates.get(currencyName.toUpperCase());
        }
        return null;
    }
}

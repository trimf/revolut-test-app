/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.mvp.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by trimf on 08/07/2017.
 */

public interface ICallback<T> {
    void onSuccess(@NonNull T data);

    void onFailure(@Nullable Throwable t);
}

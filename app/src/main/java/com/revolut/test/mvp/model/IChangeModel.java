/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.mvp.model;

import com.revolut.test.mvp.model.data.ChangeRatesData;

/**
 * Created by trimf on 08/07/2017.
 */

public interface IChangeModel {
    void getChangeRates(ICallback<ChangeRatesData> callback, String currency);
}

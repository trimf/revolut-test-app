/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.mvp.presenter;

import com.revolut.test.mvp.view.IChangeView;

/**
 * Created by trimf on 07/07/2017.
 */

public interface IChangePresenter extends IBasePresenter<IChangeView> {
    void pageSelected(int viewPagerNumber);

    void pagerFocused(int viewPagerNumber);

    void amountChanged();

}

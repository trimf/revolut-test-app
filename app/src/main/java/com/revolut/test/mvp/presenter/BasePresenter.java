/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.mvp.presenter;

import android.support.annotation.NonNull;

import java.lang.ref.WeakReference;

public abstract class BasePresenter<V> implements IBasePresenter<V>{
    private WeakReference<V> view;

    protected void resetState() {
    }

    @Override
    public void bindView(@NonNull V view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void unbindView() {
        this.view = null;
    }

    protected V view() {
        if (view == null) {
            return null;
        } else {
            return view.get();
        }
    }

    protected boolean setupDone() {
        return view() != null;
    }
}

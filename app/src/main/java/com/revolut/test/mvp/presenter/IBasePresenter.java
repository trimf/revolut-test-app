/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.revolut.test.mvp.presenter;

import android.support.annotation.NonNull;

/**
 * Created by trimf on 07/07/2017.
 */

public interface IBasePresenter<V> {
    public void bindView(@NonNull V view);
    public void unbindView();
}

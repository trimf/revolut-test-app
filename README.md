# Revolut test #

## Explicit Requirements: ##
* Your app should poll the fixer.io endpoint every 30 seconds to get the latest rates for GBP, EUR and USD. (The API provides close of day FX rates. We expect you to request the new rate every 30s. We do not expect the rate to change every 30s)
* Focus more on code design rather than UI, but if you have time at the end of the task, please try and mimic the Revolut UI to a certain extent



## Implicit Requirements ##
The code produced is expected to be of very good quality
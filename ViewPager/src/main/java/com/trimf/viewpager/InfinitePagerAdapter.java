/*
 * **********************
 * Copyright (c) 2017.
 * trimf
 * **********************
 */

package com.trimf.viewpager;

import android.view.View;
import android.view.ViewGroup;

import java.util.List;


/**
 * Created by trimf on 06/07/2017.
 */

public class InfinitePagerAdapter<T extends BasePagerItem> extends BasePagerAdapter<T> {

    private int mSize;
    private int mDeleter;

    public InfinitePagerAdapter(List<T> pagerItems) {
        super(pagerItems);
        mDeleter = 1;
        //Add pages until we have 4 elements, lifecycle of view pager is so,
        //that we should have at least 4 elements, if we create infinite scrolling view pager
        //such way
        while (mPagerItems.size() < 4) {
            int size = pagerItems.size();
            for (int i = 0; i < size; i++) {
                try {
                    mPagerItems.add((T)pagerItems.get(i).clone());
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
            }
            mDeleter++;
        }
        mSize = (Integer.MAX_VALUE / mPagerItems.size()) / 2 * mPagerItems.size() * 2;
    }


    public int getPageNumber() {
        return mPagerItems.size() / mDeleter;
    }

    public int getCurrentPage(int position) {
        return getRealPosition(position) % getPageNumber();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        int realPosition = getRealPosition(position);
        if (realPosition >= 0 && realPosition < mPagerItems.size()) {
            getPagerItem(position).destroyView();
            View view = (View) object;
            (container).removeView(view);
        }
    }

    @Override
    protected BasePagerItem getPagerItem(int position) {
        return mPagerItems.get(getRealPosition(position));
    }

    public int getRealPosition(int position) {
        return position % mPagerItems.size();
    }

    @Override
    public int getCount() {
        return mSize;
    }
}

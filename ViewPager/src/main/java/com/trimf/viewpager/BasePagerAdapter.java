package com.trimf.viewpager;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by a.baskakov on 26/09/2016.
 */

public abstract class BasePagerAdapter<T extends BasePagerItem> extends PagerAdapter {
    protected List<T> mPagerItems;

    public BasePagerAdapter(List<T> pagerItems) {
        mPagerItems = pagerItems;
    }

    @Override
    public int getCount() {
        return mPagerItems.size();
    }

    protected BasePagerItem getPagerItem(int position) {
        return mPagerItems.get(position);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View layout = getPagerItem(position).getView();
        if (layout.getParent() == null) {
            container.addView(layout);
        }
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        if (position >= 0 && position < mPagerItems.size()) {
            getPagerItem(position).destroyView();
            View view = (View) object;
            (container).removeView(view);
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public List<T> getList() {
        return mPagerItems;
    }

    public void setList(List<T> list) {
        mPagerItems = list;
    }
}


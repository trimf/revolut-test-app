package com.trimf.viewpager;

import android.content.Context;
import android.view.View;

/**
 * Created by alexey on 13/02/15.
 */
public abstract class BasePagerItem implements Cloneable {
    protected Context mContext;

    public BasePagerItem(Context context) {
        this.mContext = context;
    }

    public abstract View getView();

    public abstract void destroyView();

    public String getTitle() {
        return toString();
    }

    public BasePagerItem clone() throws CloneNotSupportedException {
        BasePagerItem basePagerItem = (BasePagerItem)super.clone();
        basePagerItem.mContext = mContext;
        return basePagerItem;
    }
}
